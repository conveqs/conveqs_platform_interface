#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Sample client for testing the Conveqs radar data connection

This module demonstrates usage of Conveqs radar data stream by making a
connection to radar channels. After subscription, the data are printed to screen.

"""

import websocket
import json

# Two channel names, covering all radars of two junctions
CHANNEL = "radar.269.*.objects_port.json,radar.270.*.objects_port.json" 
URL = "wss://twin.conveqs.fi/ws/v1/live/sensors"
TOKEN = "your-token"

# Callback functions
def on_message(ws, message):
    msg = json.loads(message)
    print("-----", msg['source'], "-----")
    print(message)

def on_error(ws, error):
    print("### WebSocket ERROR: ###")
    print(error)

def on_close(ws, close_status_code, close_msg):
    print("### WebSocket closed ###")
    print(close_status_code, close_msg)

def on_open(ws):
    print("### WebSocket connected ###")

if __name__ == "__main__":
    websocket.enableTrace(False) # set True for WebSocket debug messages
    
    api_call_url = f"{URL}?channel={CHANNEL}&token={TOKEN}"
    print("API CALL URL:", api_call_url)

    ws = websocket.WebSocketApp(
        api_call_url,
        on_open=on_open,
        on_message=on_message,
        on_error=on_error,
        on_close=on_close,
        )

    ws.run_forever()
