#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Sample client for testing the Conveqs REST API

This module demonstrates usage of Conveqs REST API by making a
connection to the REST server, getting data and printing it out.

Example:

        $ python src/test_rest_api.py
"""

import requests
import json


TEST_URL = "https://twin.conveqs.fi/rest/radars.json?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImRldnRlc3QifQ.Ey_gbjgLhr49fjAN9A_Ybv0YGIknniNGf8s71GuDHNE"

BASE_URL = "https://twin.conveqs.fi/rest"

# This token is for development testing only
# You have to use your own token provided by Conveqs
TOKEN = (
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2"
        "VybmFtZSI6ImRldnRlc3QifQ.Ey_gbjgLhr49fjAN9A"
        "_Ybv0YGIknniNGf8s71GuDHNE")

REST_PATHS = ["/radars.json", "controllers.json"]
# REST functions

def generate_rest_url(
        path,
        base_url=BASE_URL,
        token = TOKEN):
    url = base_url + "/" + path + "?token=" + token
    
    return url

def get_rest_data(url):
    "Returns REST data from given URL in json"
    response = requests.get(url)
    #msg = json.loads(message)
    #print("response:", response)
    data_in_json = response.json()
    return data_in_json


if __name__ == "__main__":
    print("Testing the Conveqs REST interface")
    for path in REST_PATHS:
        url = generate_rest_url(path)
        print(url,":")
        data = get_rest_data(url)
        print(json.dumps(data, indent=4, sort_keys=False))
        print("****")

