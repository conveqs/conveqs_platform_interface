
API DOCUMENTATION
=============================

# About
This repository contains documentation and sample codes for API calls to access the Conveqs traffic data interface. Please contact [jatkasaari@conveqs.fi](mailto:jatkasaari@conveqs.fi) for questions and access token.  


# Getting Started

## The Conveqs Platform
**The Platform is composed of:**

* **Road Network Elements**
    * Junctions
    * Road Segments connected to Junctions
    * Lanes of each Road Segment
* **Data Provider Nodes**
    * **Traffic Sensors**
        * Radars
        * Lidars  
        * HSL feed  
    * **Controllers**
        * Traffic Signal Status
        
**Radars measure and provide the following data:**

1. Vehicle movement measurements (high resolution measurement as vehicles move)  

2. Vehicle trip (summary of routes of the vehicles)  

3. Virtual Detectors for counting vehicles  

**Installed Radars**  
There are 17 radars currently available in the platform. The radars are located in Jätkäsaari area covering the corridor from Länsisatama port to the Länsiväylä motorway. Each radar covers one road segment of the junction. Locations of installed (or planned) radars are as follows:

![Figure 1: Installed radars](images/radars_with_channel_names.png)  

**Radar Configuration**  
The Smart Micro tracks moving objects appearing in it's beam covering up to eight lanes of traffic up to 150 meters from the radar location. The radar reports geolocation, speed and other attributes of the objects periodically (e.g. at 10 Hz). A typical radar setup can be seen in the following figure.

![Figure 2: Radar setting](images/radar_view.png)


**Configured Measurement Lines for Virtual Detectors**  
There are also 17 virtual measurement lines configured for virtual detectors, where each line corresponds to one radar (i.e. one road segment):

[View on google maps](https://www.google.com/maps/d/u/1/edit?mid=1wJJVg-8se6TH2C-9DWIiWE4tVYicsLY&usp=sharing)

![Figure 3: Configured measurement lines for virtual detectors](images/virtual-detectors-map.png)





## How to work with Data APIs?

You will need a valid **web-token** to call the APIs. You can request one from jatkasaari@conveqs.fi


**Typical steps to access data sources:**

1. **Call a *Metadata API* to get the list of data provider channels together with their detailed description.**  
Example: 
```
https://twin.conveqs.fi/rest/v1/metadata/sensors?token=<your-token>  
```
2. **Call a *Live Data Stream API* to subscribe to your desired topics to start receiving the live data.**  
Example: 
```
wss://twin.conveqs.fi/ws/v1/live/sensors?channel=<channel-names-separated-by-comma>&token=<your-token>  
```
  
3. **Call a *Historical Data API* to download past (historical) data of your desired topics, given a specific date-time range.**  
Example: 
```
https://twin.conveqs.fi/rest/v1/history/sensors_trips?channel=<channel-names-separated-by-comma>&from=<datetime>&to=<datetime>&token=<your-token>  
```

 
**Notes**  

* Data is provided with *publish/subscribe* method.  
* The terms "topic" and "channel" are synonymous in this document.  
* Web protocols used: 
    * WebSocket
    * HTTP REST


## Example Scripts

[Examples on how to use these APIs in Python can be found in here.](https://bitbucket.org/conveqs/conveqs_platform_interface/src/master/examples/)

REST example to call several Metadata APIs. Retrieves available radars channels and controller channels:  
```
import requests
import json

# REST API functions to be called
REST_PATHS = ["metadata/sensors", "metadata/controllers"]

BASE_URL = "https://twin.conveqs.fi/rest/v1"

# You have to use your own token provided by Conveqs
TOKEN = "your-token"

def generate_rest_url(path, base_url, token):
    "Function to formulate the URI for API call"
    url = base_url + "/" + path + "?token=" + token    
    return url

def get_rest_data(url):
    "Returns REST data from given URL in json"
    response = requests.get(url)
    # print("response:", response)
    data_in_json = response.json()
    return data_in_json


if __name__ == "__main__":
    print("Testing the Conveqs REST interface")
    
    for path in REST_PATHS:
        url = generate_rest_url(path, BASE_URL, TOKEN)
        print("Calling API:", url)
        data = get_rest_data(url)
        print("Call returned:")
        print(json.dumps(data, indent=4, sort_keys=False))
        print("----------------------------------------------------------")
        print()
```

WebSocket example to access a Live Data Stream API, to start streaming down live data from chosen radar channels. You can use astrex (*) to cover several channels. This example subscribes to two channel names (each for one junction) and covers all radars installed in those two junctions:  
```python
import websocket
import json

# Two channel names, covering all radars of two junctions
CHANNEL = "radar.269.*.objects_port.json,radar.270.*.objects_port.json"
URL = "wss://twin.conveqs.fi/ws/v1/live/sensors"
TOKEN = "your-token"

# Callback functions
def on_message(ws, message):
    msg = json.loads(message)
    print("-----", msg['source'], "-----")
    print(message)

def on_error(ws, error):
    print("### WebSocket ERROR: ###")
    print(error)

def on_close(ws, close_status_code, close_msg):
    print("### WebSocket closed ###")
    print(close_status_code, close_msg)

def on_open(ws):
    print("### WebSocket connected ###")

if __name__ == "__main__":
    websocket.enableTrace(False) # set True for WebSocket debug messages
    
    api_call_url = f"{URL}?channel={CHANNEL}&token={TOKEN}"
    print("API CALL URL:", api_call_url)

    ws = websocket.WebSocketApp(
        api_call_url,
        on_open=on_open,
        on_message=on_message,
        on_error=on_error,
        on_close=on_close,
        )

    ws.run_forever()

```



---

---


# Data APIs

**APIs are in four main categories**

1. Metadata APIs: List data providers subscription topics together with their detailed description (via HTTP REST)  

2. Live Data Stream APIs: Connect to a live data stream (via WebSocket)  

3. Historical Data APIs: Query for past historical data (via HTTP REST)   

4. Status APIs

## Metadata APIs:

### --- Traffic Sensor Channels ---
#### Description
Returns the list of topics (channels) to subscribe to.
#### API Call Format
```
https://twin.conveqs.fi/rest/v1/metadata/sensors?token=<your-token>
```
#### Returned Data
| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |  | Sensor's channel/topic name (See the naming convention below) |
| lat | float | Degrees | Sensor's location latitude degrees (WGS 84) |
| lon | float | Degrees | Sensor's location longitude degrees (WGS 84) |
| bearing | float | Degrees | Sensor's angle clockwise from north |
| timezone | string |  | Time zone where the sensor is located |
| description | string |  | Description |


### --- Virtual Detector Channels ---
#### Description
Returns the list of detectors and their topics (channels). Detectors count vehicles that pass by at a certain location in the road segment.
#### API Call Format
```
https://twin.conveqs.fi/rest/v1/metadata/detectors?token=<your-token>
```
#### Returned Data

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |   | Channel/topic name of this detector (See the naming convention below) |
| type | string |   | a) virtual: Radar-based virtual detector, b) loop: Physical loop detector |
| municipality	 | string |   | Municipality name |
| junction | string |   | Junction code |
| road_segment | string |   | Road segment name |
| distance_from_stop | integer | meters | Detector's distance from stop line | 
| lane | string |  | Lane name. Examples: A1, A2, etc.; D1, D2, etc.; A1T, D1T, etc. ([see below for naming schema description](#naming-schema-of-road-segment-lanes-at-junctions)) |
| lane_direction_to_junction | string |  | 'arriving', 'departing' |
| lane_type | string |  | 'driving', 'tram' |
| geometry | string |   | Geolocations of the start and end of the virtual loop main measurement line covering the whole road segment, covering all approaching and departing lanes |
| fixed | boolean |   | Flag showing whether this detector is finalized |
| notes | string |   | Notes |


### --- Signal Status Channels (provided by controllers) ---
#### Description
Returns the list of controllers and their topics (channels). These channels provide signal group status data.
#### API Call Format
```
https://twin.conveqs.fi/rest/v2/metadata/controllers?token=<your-token>
```
#### Returned Data
Represented as a tree, divided per junction. Each controller is connected to several signal groups where each signal group control traffic flow of one or more traffic links. Traffic link is defined as the allowed traffic flow for specific traffic type(s) (e.g. vehicles, trams, pedestrians) from one road-segment to another road-segment.

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| municipality	 | string |   | Municipality name |
| junction_code | string |   | Junction code  |
| lat | float | Degrees | Junction's location latitude degrees (WGS 84) |
| lon | float | Degrees | Junction's location longitude degrees (WGS 84) |
| controllers	| JSON object |  | List of controllers (by their IDs) installed in the current junction |
| channel | string |   | Channel/topic name of the controller |
| signal_groups	| JSON object |  | List of signal groups connected to a controller |
| controls_traffic_links	| array of JSON objects |  | List of the traffic flow link(s) controlled by a signal group |
| traffic_type	 | string |   | The traffic modality (e.g. vehicles, trams, pedestrians) allowed in this traffic flow link  |
| from_road_segment	 | string |   | Flow from which road-segment of the junction  |
| from_road_segment	 | string |   | Flow to which road-segment of the junction  |
| description	 | string |   |   |



## Live Data Stream APIs:

### --- Vehicle Movement Stream ---
#### Description
Provides high resolution point by point detection of each moving object detected. Sends several updates per second (e.g. 10 Hz).
#### API Call Format
```
wss://twin.conveqs.fi/ws/v1/live/sensors?channel=<channel-names-separated-by-comma>&token=<your-token>
```

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |  | Channel name(s) separated by “,”. No space allowed between them! |
| token | string |  | Your access token |

#### Returned Data

**Header of each message**

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| source | string |  | The source sensor's channel/topic name |
| tstamp | float | millilseconds | UNIX (epoch) timestamp in milliseconds (in UTC) |
| nobjects | int |  | Number of objects reported in this message, listed in the array below |
| objects | array of JSON string elements |  | Array of objects reported in this message |

**For each object in the array:**

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| id | int |  | Internal ID of the object, cycles from 0 to 254 |
| lat | float | Degrees (WGS 84) | Latitude of the object's current location |
| lon | float | Degrees (WGS 84) | Longitude of the object's current location |
| speed | float | m/s | Speed |
| bearing | float | Degrees | Angle (degrees) clockwise from north |
| quality | float | percentage | Detection quality determined by the radar |
| length | float | meters | Length of the object |
| class | int | enumeration ([see the descriptions below](#vehicle-classes)) | Object type determined by the radar |
| lane | int | lane id | Lane id number |
| acceleration | float | m/s^2 | Object acceleration |
| last_updated | float | seconds | Time from last time radar detected obj. |


### --- Vehicle Trip (Route) Stream ---
#### Description
One event reported per each vehicle. The object route from its appearance in the radar range until it leaves.
#### API Call Format
```
wss://twin.conveqs.fi/ws/v1/live/sensors_trips?channel=<channel-names-separated-by-comma>&token=<your-token>  
```

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |  | Channel name(s) separated by “,”. No space allowed between them! |
| token | string |  | Your access token |

#### Returned Data


| Field | Type | Unit | Description |
| --- | --- | --- | --- |
| source_id | int |  | Radar ID |
| trip_id | int |  | Unique for each radar |
| ulid | string |  | Universally Unique |
| tstamp | string | date-time | Start time of the trip |
| trav_time | float | seconds | Duration of the trip |
| trav_dist | float | meters | Euclidean trip distance |
| object_length | float | meters | Length of the object (average over all measurements of this trip) |
| class | int | enumeration ([see the descriptions below](#vehicle-classes)) | Object type determined by the radar (the most frequent 'class' seen over this trip) |
| stops | int |  | How many times the vehicle stopped during the trip due to traffic, red light, etc. |
| stuck_time | float | seconds | Assuming the vehicle stopped once or more during the trip, this value is the maximum of those delays |
| object_type | string |  | The most frequent vehicle type determined by the radar during this trip |
| lane | int |  | Lane id number |
| avg_speed | float | m/s | average speed = trav_dist/trav_time |
| route_trav_dist | float | meters | Trip distance along the vehicle's route (more accurate than trav_dist) |
| coordinates.start | float tuple | (lat, lon) coordinates | The first point where the vehicle appeared to the radar |
| coordinates.end | float tuple | (lat, lon) coordinates | The last point where the vehicle was seen by the radar |

### --- Virtual Detectors Vehicle Counting Stream  ---
#### Description
Provides individual vehicle count events.
#### API Call Format
```
wss://twin.conveqs.fi/ws/v1/live/detectors?channel=<channel-names-separated-by-comma>&token=<your-token>  
```

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |  | Channel name(s) separated by “,”. No space allowed between them! |
| token | string |  | Your access token |


#### Returned Data


| Field                | Type    | Unit    | Description                                                                                                                                               |
|----------------------|---------|---------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| channel | string  |         | Detector's channel name, that also denotes detector's location in the road network                                                                        |
| tstamp               | float  | seconds | UNIX (epoch) timestamp in seconds (in UTC)                                                                                                                 |
| tstamp_str_utc       | string  |         | Timestamp as a string in UTC                                                                                                                               |
| tstamp_str_local     | string  |         | Timestamp as a string in local time with time zone offset mentioned                                                                                          |
| vehicle              | JSON object |     | Attributes of the moving object detected in this event                                                                                                                                               |
| vehicle.class        | integer |         | Moving object's class                                                                                                                                               |
| vehicle.speed        | float  | m/s     | Speed in m/s                                                                                                                                              |
| vehicle.acceleration | float  | m/s^2   | Acceleration in m/s^2                                                                                                                                     |
| vehicle.direction    | string  |         | Vehicle movement direction in relation to the junction. Identifiable only after the vehicle has passed the detector line. Possible values: [arriving, departing, NA] |





### --- Detectors Vehicle Counting Stream  (only version 2 API) ---
#### Description
Provides individual vehicle count events, as detected by different detector types. At the moment provided by the inductive loop detectors and radar-based virtual detectors.
#### API Call Format
```
wss://twin.conveqs.fi/ws/v2/live/detectors?channel=<channel-names-separated-by-comma>&token=<your-token>  
```

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |  | Channel name(s) separated by “,”. No space allowed between them! |
| token | string |  | Your access token |


#### Returned Data


| Field                | Type    | Unit    | Description                                                                                                                                               |
|----------------------|---------|---------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| channel | string  |         | Detector's channel name, that also denotes detector's location in the road network                                                                        |
| tstamp               | float  | seconds | UNIX (epoch) timestamp in seconds (in UTC)                                                                                                                 |
| tstamp_str_utc       | string  |         | Timestamp as a string in UTC                                                                                                                               |
| tstamp_str_local     | string  |         | Timestamp as a string in local time with time zone offset mentioned                                                                                          |
| occupancy_duration | float | seconds | (Only for inductive loop detectors)  How long the detector was occupied by a vehicle on top of it |
| vehicle              | JSON object |     | Attributes of the moving object detected in this event                                                                                                                                               |
| vehicle.class        | integer |         | Moving object's class  |
| vehicle.speed        | float  | m/s     | Speed in m/s                                                                                                                                              |
| vehicle.acceleration | float  | m/s^2   | Acceleration in m/s^2                                                                                                                                     |
| vehicle.direction    | string  |         | Vehicle movement direction in relation to the junction. Identifiable only after the vehicle has passed the detector line. Possible values: [arriving, departing, NA] |




### --- Signal Status Data ---
#### Description
Provides real-time status of traffic signal groups.
#### API Call Format
```
wss://twin.conveqs.fi/ws/v2/live/signals?channel=<channel-names-separated-by-comma>&token=<your-token>  
```

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |  | Channel name(s) separated by “,”. No space allowed between them! |
| token | string |  | Your access token |


#### Returned Data


| Field                | Type    | Unit    | Description                                                                                                                                               |
|----------------------|---------|---------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| channel | string  |         | Source controller's channel name |
| tstamp | float  | seconds | UNIX (epoch) timestamp in seconds (in UTC) |
| signal_groups | JSON object |     | List of the signal groups connected to this controller |
| signal_groups.group | integer |     | Signal group's identifier, as listed by the metadata |
| signal_groups.substate | string |     | Current substate of the group |
| signal_groups.state | string |     | Current (main) state of the group shown as "red", "green", "redamber, etc. |



## Historical Data APIs:

### --- Vehicle Trip (Route) Historical Data ---
#### Description
Retrieve historical data of individual vehicle trips (routes) as recorded by sensors near junctions.
#### API Call Format
```
https://twin.conveqs.fi/rest/v1/history/sensors_trips?channel=<channel-names-separated-by-comma>&from=<date-time>&to=<date-time>&token=<token>
```

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |  | Sensor channel name(s) separated by “,”. No space allowed between them! |
| from | date-time string |  | Limit the results to records starting from this timestamp (inclusive of this value). Given in the *local time zone* of the target sensor channel, *without* writing the time zone offset. |
| to | date-time string |  | Limit the results to records ending at this timestamp (excluding this value). Given in the *local time zone* of the target sensor channel, *without* writing the time zone offset. |
| token | string |  | Your access token |


Example API call to return routes recorded during an *entire day* (21.3.2024). Date range parameters as:

* From 2024-3-21 00:00:00
* To   2024-3-22 00:00:00

```
https://twin.conveqs.fi/rest/v1/history/sensors_trips?from=2024-3-21T00:00:00&to=2024-3-22T00:00:00&channel=radar.269.1.objects_port.json&token=<token>
```


#### Returned Data


| Field               | Type    | Unit           | Description                                                                                                           |
|---------------------|---------|----------------|-----------------------------------------------------------------------------------------------------------------------|
| channel        | string  |                | Source sensor's channel from which the trip data was recorded.                                                         |
| trip_id | int |  | Unique for each radar |
| ulid | string |  | Universally Unique |
| tstamp_utc | string | date-time | Start time of the trip in UTC |
| tstamp_local | string | date-time | Start time of the trip in the local time zone of the sensor |
| class | int | enumeration ([see the descriptions below](#vehicle-classes)) | Object type determined by the radar (the most frequent 'class' seen over this trip) |
| object_length | float | meters | Length of the object (average over all measurements of this trip) |
| stops | int |  | How many times the vehicle stopped during the trip due to traffic, red light, etc. |
| stuck_time | float | seconds | Assuming the vehicle stopped once or more during the trip, this value is the maximum of those delays |
| lane           | integer |                | The lane ID number .                                                                              |
| trav_dist | float | meters | Trip distance along the vehicle's route (more accurate than trav_dist) |
| trav_time      | float | seconds | Recorded travel duration.                                                                                  |
| stops          | integer |                | The number of stops during the trip.                                                                                  |
| stuck_time     | float  | seconds        | Total time spent in by stops (idle), if any. |
| coordinates | JSON object |   | Start and end coordinates of the recorded route. |
| coordinates.start | float tuple | (lat, lon) coordinates (WGS 84) | The first point where the vehicle appeared to the radar |
| coordinates.end | float tuple | (lat, lon) coordinates (WGS 84) | The last point where the vehicle was seen by the radar |



### --- Detectors Historical Data ---
#### Description
Retrieve detectors historical data with *aggregated* vehicle counts over given time intervals e.g. every 15 minutes.
#### API Call Format
```
https://twin.conveqs.fi/rest/v1/history/detectors?channel=<channel-names-separated-by-comma>&from=<date-time>&to=<date-time>&interval=15&token=<token>
```

| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |  | Detector channel name(s) separated by “,”. No space allowed between them! |
| from | date-time string |  | Limit the results to records starting from this timestamp (inclusive of this value). Given in the *local time zone* of the target sensor channel, *without* writing the time zone offset. |
| to | date-time string |  | Limit the results to records ending at this timestamp (excluding this value). Given in the *local time zone* of the target sensor channel, *without* writing the time zone offset. |
| interval | integer | minutes | The time interval at which the vehicle counts are to be aggregated |
| count_per_object_type | boolean | true/false (assumed false if not passed) | Set this flag in order to count per object type (per 'class') in each interval |
| token | string |  | Your access token |

Example API call to return counts for an *entire day* (12.5.2024), aggregated at 15 minute intervals and *per vehicle class*, sourced from *two different* virtual detector channels. Date range parameters as:

- From 2024-5-12 00:00:00
- To   2024-5-13 00:00:00

```
https://twin.conveqs.fi/rest/v1/history/detectors?from=2024-5-12T00:00:00&to=2024-5-13T00:00:00&interval=15&count_per_object_type=true&channel=vdetector.fi.helsinki.267.mechelininkatu_north.20m.D1T.json,vdetector.fi.helsinki.263.lansivayla_north.20m.A1.json&token=<token>  
```

#### Returned Data


| Name | Type | Unit | Description |
| --- | --- | --- | --- |
| channel | string |  | Name of the detector topic/channel |
| interval_start | date-time string |  | Listed date-times include this value too |
| interval_end | date-time string |  | Listed date-times are exclusive of this value |
| vehicle_count | integer |  | Number of vehicles counted during this interval by the detector |
| class |  |  | N/A for now |
| speed_avg | float | m/s | Mean value of observed vehicle speeds |
| acceleration_avg | float | m/s^2 | Mean value of observed vehicle accelerations |


## Status APIs:

### --- Sensor Status ---
#### Description
Whether each sensor is on (active and sending data) or off (inactive).
#### API Call Format
```
https://twin.conveqs.fi/rest/v1/status/sensors?token=<token>
```
#### Returned Data


| Field      | Type    | Unit      | Description |
|------------|---------|-------------|------------------------------------------------------------------------------------------------------|
| channel    | string  |             | Sensor's channel name for subscription |
| up_status  | integer |             | Whether up or down at the moment. Possible values: 0 (down), 1 (up) |
| since      | string  | date-time   | Timestamp when status changed |
| duration   | string  | date-time interval | Duration of the current status                                                                               |
| notes      | string  |             | Manually added notes e.g. 'Edge-unit out and under maintenance' |


---

---


# Extra Information on Data Fields

## Time Zone Behavior of Date-time API Parameters 
The user always specifies the date-time input parameters in the *local time zone*, that is, time zone of the target sensor channel, and does not need to worry about time zone conversion, UTC, etc. 

In other words, the API assumes that the *from* and *to* timestamp parameters are given in the local time zone of each related sensor, and also automatically considers *daylight saving offsets* based on the given dates.


## Vehicle Class Enumeration

| Class id | Length (m) | Classification | Notes |
| --- | --- | --- | --- |
| 0 | unknown | Undefined | Fixed length |
| 1 | 1.0 | Pedestrian | Fixed length |
| 2 | 1.6 | Bicycle | Fixed length |
| 3 | 2.6 | Motorcycle | Fixed length |
| 4 | [4.6, 5.4] | Passenger car | Estimated length |
| 6 | [5.6, 8.8] | Delivery/pickup | Estimated length |
| 7 | [9.0, 13.8] | Short Truck | Estimated length |
| 8 | >= 14.0 | Long Truck | Estimated length |


## Naming Schema of Channel Names
**Sensors Channel Naming Formats**

HSL sourced: <sensing-source>.<city-area>.<transportation-mode>.json  
Lidars: <sensor-type>.<city-area>.<sensor-ID>.json  
Others (including radars): <sensor-type>.<junction-code>.<sensor-ID-at-junction>.objects_port.json


**Detectors Channel Naming Formats**

*<data-source-type>.<location-on-road-network>.<lane-label>.json*  

* <data-source-type>: 'vdetector' denotes a 'virtual detector' sourced e.g. from a radar, while 'loopdetector' denotes a physicall loop detector, etc.
* <location-on-road-network> format: <cuontry-code>.<city-name>.<junction-code>.<road-segment-name_side-to-junction>.<distance-from-stop-line>

Examples:  
vdetector.fi.helsinki.267.mechelininkatu_north.20m.A1T.json  
vdetector.fi.helsinki.267.mechelininkatu_north.20m.A2.json  
.  
.  
.  
vdetector.fi.helsinki.267.mechelininkatu_north.20m.D1T.json  
vdetector.fi.helsinki.267.mechelininkatu_north.20m.D2.json  
.  
.  
.  




## Naming Schema of Road Segment Lanes at Junctions

**Lane name format is as follows:**  
*<Letter><Number><Optional Letter Denoting Lane Type>*


* <Letter>:

    * 'A': Approaching the junction
    * 'D': Departing from the junction

* <Number>:

    + For two-way roads, there is an *imaginary* reference lane at the center of the road, numbered 0.
    + Lane number counting starts from the cetner lane, in each A and D direction. Lanes numbered 1 are the innermost lanes on both sides of the *imaginary* center lane, and the highest numbers denote the outermost lanes. Examples: A1, A2, etc., and D1, D2, D3 etc.
    + In case of one-way roads, the imaginry reference lane (number 0) is located leftmost in the road's direction. One way road lane names start either only with A or only with D depending on the road's direction relative to the junction.

* <Optional Letter Denoting Lane Type>:

    * 'T': Tram lane


**Examples:**

A1, A2, D1  
A1T, D1, D2, D3T

In one of the junction road segments:  
A1 denotes the first approaching vehicle lane to the right of the cetner (0) lane  
A2 denotes the second approaching vehicle lane to the right of the cetner (0) lane  

In another junction road segment:  
A1T denotes the first approaching lane which is a tram lane  
D1T denotes the first departing lane (to the left of the center lane), which is a tram lane 


Figure below illustrates the current lane labels for the *Tyynenmerenkatu* road-segment connected to the *junction 270* in Jätkäsaari area of Helsinki.

![Figure: Lane labeling example for one road segment in junction 270](images/lanes-junction-270-road-tyynenmerenkatu-small.png)  




---

---


# JSON Schemas
## Metadata APIs
[Traffic Sensor Channels](https://bitbucket.org/conveqs/conveqs_platform_interface/src/master/json_schemas/metadata-sensors.json)

[Virtual Detector Channels](https://bitbucket.org/conveqs/conveqs_platform_interface/src/master/json_schemas/metadata-detectors.json)

## Live Data Stream APIs
[Vehicle Movement Stream](https://bitbucket.org/conveqs/conveqs_platform_interface/src/master/json_schemas/live-sensors.json)

[Vehicle Trip (Route) Stream](https://bitbucket.org/conveqs/conveqs_platform_interface/src/master/json_schemas/live-sensors_trips.json)

[Virtual Detectors Vehicle Counting Stream](https://bitbucket.org/conveqs/conveqs_platform_interface/src/master/json_schemas/live-detectors.json)

 
## Historical Data APIs
[Vehicle Trip (Route) Historical Data](https://bitbucket.org/conveqs/conveqs_platform_interface/src/master/json_schemas/history-sensors_trips.json)

[Virtual Detectors Historical Data](https://bitbucket.org/conveqs/conveqs_platform_interface/src/master/json_schemas/history-detectors.json)


## Status APIs
[Sensor Status](https://bitbucket.org/conveqs/conveqs_platform_interface/src/master/json_schemas/status-sensors.json)


---

---


# Contact
For access token, questions or feedback please contact [jatkasaari@conveqs.fi](mailto:jatkasaari@conveqs.fi).


